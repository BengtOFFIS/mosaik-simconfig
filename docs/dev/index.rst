=========================
Developer's Documentation
=========================

Contents:

.. toctree::
   :maxdepth: 1

   design
   setup
   release_process
   requirements
   design-decisions
   logo-ci

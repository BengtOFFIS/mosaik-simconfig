from mosaik_api import Simulator


class MockSimulator(Simulator):
    def __init__(self):
        meta = {
            'models': {
                'MockModel': {
                    'public': True,
                    'params': [],
                    'attrs': [
                        'my_attribute',  # output
                    ],
                },
            },
        }
        super().__init__(meta)

    def step(self, time, inputs):
        pass

    def get_data(self, outputs):
        pass

    def create(self, num, model, **model_params):
        pass
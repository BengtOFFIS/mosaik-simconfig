import os

import mosaik_api

import mosaik
from mosaik.sim_config import SimConfig
from tests.mock.simulator import MockSimulator


def test_instantiation(sim_config):
    assert sim_config is not None
    assert type(sim_config) is SimConfig


def test_add_in_process(sim_config):
    sim_config.add_in_process(simulator=MockSimulator)

    expected = {
        'MockSimulator': {
            'python': 'tests.mock.simulator:MockSimulator'
        }
    }

    assert sim_config == expected

    world = mosaik.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)


def test_add_in_subprocess_python(sim_config):
    sim_config.add_in_subprocess_python(simulator=MockSimulator)

    expected = \
        {'MockSimulator':
            {
                'cwd': '.',
                'cmd': 'C:\\Users\\blueers\\Downloads\\mosaik\\mosaik-SimConfig\\venv\\Scripts\\python3.exe '
                       'C:\\Users\\blueers\\Downloads\\mosaik\\mosaik-SimConfig\\tests\\mock\\simulator.py '
                       '%(addr)s',
                'env': {}
            }
        }

    # Assert current working directory
    assert sim_config['MockSimulator']['cwd'] == expected['MockSimulator']['cwd']

    # Assert command python executable
    python_actual = os.path.splitext(os.path.basename(sim_config['MockSimulator']['cmd'].split()[0]))[0].rstrip('3')
    python_expected = os.path.splitext(os.path.basename(expected['MockSimulator']['cmd'].split()[0]))[0].rstrip('3')
    assert python_actual == python_expected

    # Assert command simulator path
    simulator_path_actual = os.path.basename(sim_config['MockSimulator']['cmd'].split()[1])
    simulator_path_expected = os.path.basename(expected['MockSimulator']['cmd'].split()[1])
    assert simulator_path_actual == simulator_path_expected

    # Assert command address and port
    address_and_port_actual = sim_config['MockSimulator']['cmd'].split()[2]
    address_and_port_expected = expected['MockSimulator']['cmd'].split()[2]
    assert address_and_port_actual == address_and_port_expected

    # Assert environment variables
    assert sim_config['MockSimulator']['env'] == expected['MockSimulator']['env']

    # Use
    world = mosaik.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)


def test_add_in_subprocess_java(sim_config):
    sim_config.add_in_subprocess_java(
        java='java.exe',
        simulator_jar='MockSimulator.jar',
        simulator_class='com.example.java.simulator.MockSimulator'
    )

    print(dict(sim_config))

    expected = {
        'MockSimulator': {
            'cmd': 'java.exe -cp MockSimulator.jar com.example.java.simulator.MockSimulator %(addr)s',
            'cwd': '.',
            'env': {},
        }
    }

    # Assert current working directory
    assert sim_config['MockSimulator']['cwd'] == expected['MockSimulator']['cwd']

    # Assert command java executable
    java_actual = os.path.splitext(os.path.basename(sim_config['MockSimulator']['cmd'].split()[0]))[0]
    java_expected = os.path.splitext(os.path.basename(expected['MockSimulator']['cmd'].split()[0]))[0]
    assert java_actual == java_expected

    # Assert command simulator path
    simulator_path_actual = os.path.basename(sim_config['MockSimulator']['cmd'].split()[1])
    simulator_path_expected = os.path.basename(expected['MockSimulator']['cmd'].split()[1])
    assert simulator_path_actual == simulator_path_expected

    # Assert command address and port
    address_and_port_actual = sim_config['MockSimulator']['cmd'].split()[2]
    address_and_port_expected = expected['MockSimulator']['cmd'].split()[2]
    assert address_and_port_actual == address_and_port_expected

    # Assert environment variables
    assert sim_config['MockSimulator']['env'] == expected['MockSimulator']['env']

    # Use
    world = mosaik.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)


def test_add_in_subprocess_executable_standalone(sim_config):
    sim_config.add_in_subprocess_executable_standalone(
        simulator_exe='MockSimulator.exe',
    )
    print(dict(sim_config))

    expected = {
        'MockSimulator': {
            'cmd': './MockSimulator.exe %(addr)s',
            'cwd': '.',
            'env': {},
        }
    }

    # Assert current working directory
    assert sim_config['MockSimulator']['cwd'] == expected['MockSimulator']['cwd']

    # Assert command executable
    executable_actual = os.path.splitext(os.path.basename(sim_config['MockSimulator']['cmd'].split()[0]))[0]
    executable_expected = os.path.splitext(os.path.basename(expected['MockSimulator']['cmd'].split()[0]))[0]
    assert executable_actual == executable_expected

    # Assert command address and port
    address_and_port_actual = sim_config['MockSimulator']['cmd'].split()[1]
    address_and_port_expected = expected['MockSimulator']['cmd'].split()[1]
    assert address_and_port_actual == address_and_port_expected

    # Assert environment variables
    assert sim_config['MockSimulator']['env'] == expected['MockSimulator']['env']

    # Use
    world = mosaik.World(sim_config=sim_config)

    end_in_seconds = 1
    world.run(until=end_in_seconds)


def main():
    return mosaik_api.start_simulation(MockSimulator(), 'Mock Simulator')


if __name__ == '__main__':
    main()

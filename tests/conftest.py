from pytest import fixture

from mosaik.sim_config import SimConfig


@fixture
def sim_config() -> SimConfig:
    return SimConfig()